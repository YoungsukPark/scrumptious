from django.contrib import admin
# First step: Import the model you wish to register
from recipes.models import Recipe, RecipeStep, Ingredient

# Register your models here.

@admin.register(Recipe)
# Register the model using a class that inherits
# from admin.ModelAdmin
class RecipeAdmin(admin.ModelAdmin):
    # Add a list_display property that is
    # a tuple () of model properties you'd
    # like to see in the admin list view
    list_display = (
        "title",
        "id",
        "picture",
        "description",
        "created_on",
    )

@admin.register(RecipeStep)
class RecipeStepAdmin(admin.ModelAdmin):
    list_display = (
        "step_number",
        "instruction",
        "id",
    )

@admin.register(Ingredient)
class IngredientAdmin(admin.ModelAdmin):
    list_display = (
        "food_item",
        "amount",
    )
