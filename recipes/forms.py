from django.forms import ModelForm
from recipes.models import Recipe

# Create modelForm class that inherits from
# ModelForm
class RecipeForm(ModelForm):
    # All of your forms will have this Meta class
    # It is an Inner Class and it is used to customize
    # the form
    class Meta:
        model = Recipe
        fields = (
            "title",
            "picture",
            "description",
        )
