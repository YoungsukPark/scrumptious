# Generated by Django 4.1.5 on 2023-01-17 23:36

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("recipes", "0008_recipeingredients"),
    ]

    operations = [
        migrations.RenameModel(
            old_name="RecipeIngredients",
            new_name="Ingredients",
        ),
    ]
