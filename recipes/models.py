from django.db import models
from django.conf import settings

# Create your models here.
# Create a class that inherits from
# models.Model

class Recipe(models.Model):
    # Add properties to our model
    title = models.CharField(max_length=200)
    picture = models.URLField(blank=True)
    description = models.TextField()
    created_on = models.DateTimeField(auto_now_add=True)

    author = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="recipes",
        on_delete=models.CASCADE,
        null=True,
    )

    def __str__(self):
        return self.title

# we need to add an author
# first argument
# is always the model to whicho ur model is related
# we also need the on_delete
# CASCADE deletion strategy
# we also need a null property
# null property tells Django its okay if we don't
# have anything in this column


class RecipeStep(models.Model):
    # Add couple of properties
    step_number = models.PositiveSmallIntegerField(null=True)
    instruction = models.TextField()
    recipe = models.ForeignKey(
        Recipe,
        related_name="steps",
        on_delete=models.CASCADE,
    )

    class Meta:
        ordering = ["step_number"]


class Ingredient(models.Model):
    amount = models.CharField(max_length=100)
    food_item = models.CharField(max_length=100)
    recipe = models.ForeignKey(
        Recipe,
        related_name="ingredient",
        on_delete=models.CASCADE,
    )

    class Meta:
        ordering = ["food_item"]
